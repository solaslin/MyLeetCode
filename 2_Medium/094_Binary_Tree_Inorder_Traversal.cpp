//
// https://leetcode.com/problems/binary-tree-inorder-traversal/description/
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> ret;
        
        //
        // My iterative version:
        //
        if (!root) return ret;
        
        stack<TreeNode*> traversal;
        traversal.push(root);
        //cout << "push root:" << root->val << endl;
        
        while ( !traversal.empty() )
        {
            root = traversal.top();
            while( root->left != nullptr )
            {
                traversal.push(root->left);                
                root = root->left;         
                //cout << "push left:" << root->val << endl;                
            }
            
            while (root->right == nullptr)
            {
                //cout << "1. pop and traverse node without right:" << traversal.top()->val << endl;   
                ret.push_back( traversal.top()->val );               
                traversal.pop();                                        
                
                if (traversal.empty()) return ret;                    
                root = traversal.top();
            }
            
            //cout << "2. pop and traverse node with right:" << root->val << endl;   
            ret.push_back( root->val );    
            traversal.pop();                               
            
            
            if ( !traversal.empty()) 
                traversal.top()->left = NULL;
            
            traversal.push(root->right);
            //cout << "push right:" << root->right->val << endl;                    
        }
        
        return ret;
        
    }
};