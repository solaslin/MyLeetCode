//
// https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/description/
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        vector<vector<int>> ret;
        if (!root) return ret;
        
        deque<TreeNode*> bfsList;
        bfsList.push_back(root);
        bool curIsLeft2Right = true;  // init: root is l-to-r, next is r-to-l
        
        while ( !bfsList.empty() )
        {
            vector<int> level;
            int len = bfsList.size();
            for (int i = 0; i < len; ++i)
            {
                if (curIsLeft2Right)
                {
                    // traverse order: front (left first) 
                    root = bfsList.front();                    
                    level.push_back(root->val);
                    bfsList.pop_front();
                    // next level: append to back, so left first then right
                    if (root->left)  bfsList.push_back(root->left);
                    if (root->right) bfsList.push_back(root->right);
                }
                else    // cur level = right to left
                {
                    // traverse order: back (right first) 
                    root = bfsList.back();                    
                    level.push_back(root->val);
                    bfsList.pop_back();
                    // next level: append to front, so right first then left
                    if (root->right) bfsList.push_front(root->right);
                    if (root->left)  bfsList.push_front(root->left);
                }                
            }
            
            curIsLeft2Right = !curIsLeft2Right;             
            ret.push_back(level);
        }
        
        return ret;
    }
};