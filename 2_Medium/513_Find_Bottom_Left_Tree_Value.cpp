//
// https://leetcode.com/problems/find-bottom-left-tree-value/description/
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int findBottomLeftValue(TreeNode* root) {
        vector<TreeNode*> *current = new vector<TreeNode*>();
        vector<TreeNode*> *nexts = new vector<TreeNode*>();
        
        int leftMost = root->val;
        
        current->push_back(root);
        int len = current->size();
        while ( len )
        {            
            nexts->clear();
            TreeNode* firstOne = nullptr;
            for (int i = 0; i < len; ++i)
            {
                if ((*current)[i]->left)
                {
                    nexts->push_back((*current)[i]->left);
                    if (!firstOne) firstOne = (*current)[i]->left;
                }
                if ((*current)[i]->right) 
                {
                    nexts->push_back((*current)[i]->right);
                    if (!firstOne) firstOne = (*current)[i]->right;
                }
            }
            
            if (firstOne) leftMost = firstOne->val;
            
            vector<TreeNode*> *temp = current;
            current = nexts;
            nexts = temp;
            
            len = current->size();
        }
        
        return leftMost;
    }
};