//
// https://leetcode.com/problems/my-calendar-ii/description/
//

typedef std::pair<int,int> Period;

bool GetOverlapRange(const Period& period1, const Period& period2, Period& result)
{
    result.first = max( period1.first, period2.first);
    result.second = min( period1.second, period2.second);    
    return ( result.first < result.second );
}

class MyCalendarTwo {
public:
    MyCalendarTwo() {
    }
    
    bool book(int start, int end) {
        
        Period newPeriod(start, end);
        Period overlapped;
        
        for ( auto& doubleBooked : _boockedTwice)
        {
            if ( GetOverlapRange(doubleBooked, newPeriod, overlapped) )
                return false;
        }
        
        
        for ( auto& singleBooked : _bookedOnce)
        {
            // early out: this seems slower
            //if (start > singleBooked.second) break;
            
            if ( GetOverlapRange(singleBooked, newPeriod, overlapped) )
            {
                _boockedTwice.push_back(overlapped);
            }
        }
        
        _bookedOnce.push_back(newPeriod);
        
        // early out: this seems a performance minus 
        //_bookedOnce.sort([](const Period & a, const Period & b) { return a.second > b.second; });
        
        return true;
    }
    
private:
    std::list<Period> _bookedOnce;
    std::list<Period> _boockedTwice;
};

/**
 * Your MyCalendarTwo object will be instantiated and called as such:
 * MyCalendarTwo obj = new MyCalendarTwo();
 * bool param_1 = obj.book(start,end);
 */