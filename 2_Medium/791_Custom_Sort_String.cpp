//
// https://leetcode.com/problems/custom-sort-string/description/
//

class Solution {
public:
    string customSortString(string S, string T) {
        
        vector<int> priority(26,0);
        // store the priority of possible 26 char
        for (int id = 0; id < S.size(); ++id)
            priority[ S[id] - 'a'] = id;
                
        // each element = { priority, id_at_T }
        vector<pair<int, int>> strT_priority(T.size()); 
        for (int id = 0; id < T.size(); ++id)
            strT_priority[id] = pair<int, int>{ priority[ T[id] - 'a' ], id};
        
        sort(strT_priority.begin(), strT_priority.end());
        
        string ret = "";
        // iterate the sorted pair: get the "id_at_T" to fetch the char in T
        for (auto& pair : strT_priority )
            ret += T[ pair.second ];
        
        return ret;
    }
};