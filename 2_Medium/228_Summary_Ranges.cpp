//
// https://leetcode.com/problems/summary-ranges/description/
//

class Solution {
public:
    vector<string> summaryRanges(vector<int>& nums) {
        vector<string> outString;
        
        size_t numLens = nums.size();
        if (numLens == 0) return outString;
                
        int minInt,maxInt;
        int curInt, nextInt;
        minInt = curInt = nextInt = nums[0];        
        string rangeStr = std::to_string(minInt);        
        
        for (size_t i = 0; i < numLens-1; ++i)
        {
            curInt = nums[i];
            nextInt = nums[i+1];
            if (nextInt != curInt + 1)
            {
                // finish current range
                maxInt = curInt;
                if (maxInt != minInt)
                {
                    rangeStr = rangeStr + "->" + std::to_string(maxInt);                    
                }
                outString.push_back(rangeStr);
                
                // reset next range
                minInt = nextInt;
                rangeStr = std::to_string(minInt);
            }
        }
        
        maxInt = nextInt;
        if (maxInt != minInt)
        {
            rangeStr = rangeStr + "->" + std::to_string(maxInt);            
        }
        outString.push_back(rangeStr);
        
        return outString;
    }
};