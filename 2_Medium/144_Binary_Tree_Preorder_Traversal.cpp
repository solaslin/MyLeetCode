//
// https://leetcode.com/problems/binary-tree-preorder-traversal/description/
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> preorderTraversal(TreeNode* root) {
        
        // recursive solution is simple...
        
        // iterative solution:
        // inspired by quiz 94: Binary Tree Inorder Traversal        
        vector<int> ret;
        if (!root) return ret;
        
        stack<TreeNode*> traversal;
        while ( root || !traversal.empty() )
        {
            while ( root )
            {
                traversal.push(root);
                ret.push_back( root->val );
                root = root->left;
            }

            root = traversal.top()->right;
            traversal.pop();
        }
        
        return ret;
    }
};