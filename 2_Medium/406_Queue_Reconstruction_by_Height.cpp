//
// https://leetcode.com/problems/queue-reconstruction-by-height/description/
//

static const auto _____ = []()
{
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
    return nullptr;
}();

class Solution {
public:
    vector<pair<int, int>> reconstructQueue(vector<pair<int, int>>& people) {
        
        int len = people.size();
        if (len <= 1) return people;
        
        vector<pair<int, int>> res;
        vector<int> gequal(len,0);
        
        // sort
        sort( people.begin(), people.end() );
        
        // # of former elements >= itself
        int counter = 0;
        for (int i = 1; i <len; ++i )
        {
            if ( people[i-1].first == people[i].first)
                gequal[i] = ++counter;
            else
                counter = 0;            
            //std::cout << gequal[i] << std::endl;
        }
         
        for ( int i = len-1; i >=0; --i )
        {            
            //std::cout << "shift = " << max( 0, people[i].second - gequal[i]) << std::endl;
            res.insert( res.begin() + max( 0, people[i].second - gequal[i]) , people[i]);
        }
        
        return res;
    }
};