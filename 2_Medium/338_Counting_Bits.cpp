//
// https://leetcode.com/problems/counting-bits/description/
//

class Solution {
public:
    vector<int> countBits(int num) {
        vector<int> ret(num+1,0);
        
        // start with a brutal-force....
        for (int i = 0; i <= num; ++i)      // iter: N
        {
            int counts = 0;
            int x = i;
            while (x)                       // iter: sizeof(integer)
            {
                counts += (x & 0x1);
                x >>= 1;
            }
            ret[i] = counts;
        }
        
        return ret;
    }
};