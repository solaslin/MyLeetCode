//
// https://leetcode.com/problems/add-two-numbers/description/
//

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        
        ListNode *retRoot = new ListNode(0);
        ListNode *curNode = retRoot;
        int carry = 0;
        while ( l1 || l2 || carry != 0 )
        {
            carry += ((l1)? l1->val : 0) + ((l2)? l2->val : 0);                    
            curNode->next = new ListNode( carry%10 );
            curNode = curNode->next;                  
            l1 = (l1)? l1->next : l1;
            l2 = (l2)? l2->next : l2;
            carry /= 10;
        }
        return retRoot->next;
    }
};