//
// https://leetcode.com/problems/bulb-switcher/description/
//

/*
bool isSwitchedOddTime(int i)
{
    int times = 0;
    for ( int j = 1; j <= i; ++j)
    {
        if (i % j == 0) ++times;
    }
    
    return (times % 2) == 1;
}

class Solution {
public:
    int bulbSwitch(int n) {
        int numOfLightsOn = 0;
        
        for (int i = 1; i <= n; ++i)
        {
            if ( isSwitchedOddTime(i) )
                ++numOfLightsOn;
        }
        
        return numOfLightsOn;
    }
};
*/
class Solution {
public:
    int bulbSwitch(int n) {
        return (int)sqrt(n);
    }
};