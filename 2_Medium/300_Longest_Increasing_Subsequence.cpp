//
// https://leetcode.com/problems/longest-increasing-subsequence/description/
//

static int x = []() { 
std::ios::sync_with_stdio(false); 
cin.tie(NULL); 
return 0; 
}();

class Solution {
public:
    int lengthOfLIS(vector<int>& nums) 
    {        
        const int len = nums.size();
        if (!len) return 0;
                
        vector<int> increasingTimes( len, 0 );        
        int ret = 0;
        
        for ( int i = 0; i < len; ++i )
        {
            for ( int j = 0; j < i; ++j )
            {
                if ( nums[j] < nums[i] )    // j -> i is increasing
                    increasingTimes[i] = max(increasingTimes[i], increasingTimes[j] + 1);
            }
            
            ret = max(ret, increasingTimes[i]);
        }
        return ret + 1;
    }
};