//
// https://leetcode.com/problems/perfect-number/description/
//

class Solution {
public:
    bool checkPerfectNumber(int num) 
    {        
        if (num <= 1) return false;
        int bound = (int)sqrt((float)num);
        //std::cout << bound << std::endl << std::endl;
        int testSum = 1;
        for (int i = 2; i <= bound; ++i)
        {
            if (num % i == 0)
            {
                //std::cout << i << std::endl;
                //int q = num / i;
                //testSum += ((i == q)? i : i+q);       // I think this is more considerable
                testSum += (i + num/i);
            }
        }
        
        //std::cout << std::endl << testSum;
        return testSum == num;
    }
};