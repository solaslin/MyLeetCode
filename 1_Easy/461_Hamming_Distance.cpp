//
// https://leetcode.com/problems/hamming-distance/description/
//

static int x = []() { 
std::ios::sync_with_stdio(false); 
cin.tie(NULL); 
return 0; 
}();

class Solution {
public:
    int hammingDistance(int x, int y) {
        int XOR = x ^ y;
        int ret = 0;
        for ( int i = 0; i < 32; ++i )
        {            
            ret += XOR & 1;
            XOR >>= 1;
        }
        
        return ret;
    }
};