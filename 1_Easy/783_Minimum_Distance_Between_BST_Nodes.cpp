//
// https://leetcode.com/problems/minimum-distance-between-bst-nodes/description/
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int minDiffInBST(TreeNode* root) {
        InorderTraverse(root);
        return _min;
    }
    
    void InorderTraverse(TreeNode* node)
    {
        if (!node) return;
        
        InorderTraverse(node->left);
        _min = min(_min, abs(node->val - _prev));
        _prev = node->val;
        //cout << _prev << endl;
        InorderTraverse(node->right);
    }
    
private:
    int _min = std::numeric_limits<int>::max();
    int _prev = std::numeric_limits<int>::max();
};