//
// https://leetcode.com/problems/power-of-four/description/
//

class Solution {
public:
    bool isPowerOfFour(int num) {
        if( (num <= 0) || num&(num-1)) return false;      // have more than 1 bit is set 
        
        return !( (int)log2(num) % 2);
    }
};