//
// https://leetcode.com/problems/intersection-of-two-arrays/description/
//

class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        std::map<int,char> existing;
        
        std::vector<int> ret;        
        for ( auto i : nums1 )
            existing[i] = 1;
        
        for ( auto i : nums2 )
        {
            if ( existing.count(i)) 
            {
                existing.erase(i);
                ret.push_back(i);
            }
        }
        
        return ret;
    }
};