//
// https://leetcode.com/problems/reverse-string/description/
//

class Solution {
public:
    string reverseString(string s) {
        
        int len = s.length();
        
        char * cstr = new char[len+1];
        cstr[len] = '\0';
        for ( int i = 0; i < len; ++i )
        {            
            cstr[i] = s[ len - 1 - i ];
            //std::cout << cstr[i] << std::endl;
        }
        
        //std::cout << string(cstr) << std::endl;
        return string(cstr);
    }
};