//
// https://leetcode.com/problems/sqrtx/description/
//

static int x=[](){
    // toggle off cout & cin, instead, use printf & scanf
    std::ios::sync_with_stdio(false);
    // untie cin & cout
    cin.tie(NULL);
    return 0;
}();

class Solution {
public:
    int mySqrt(int x) {
        if (!x)     return 0;
        if (x==1)   return 1;
        
        // max possible = sqrt(MAX_INT) = 46340
        // set max end = 46341 
        unsigned int start = 0, end = 46341;
        unsigned mid = (start + end) >> 1;
        //cout << mid << endl;
        while (1)
        {
            unsigned square = mid * mid;
            //cout << endl << square << endl;
            if (square == x) return mid;
        
            if (square > x)
            {
                end = mid;
                mid = (start + end) >> 1;
                //cout << "bigger:" << mid << endl;
            }
            else
            {
                start = mid;
                mid = (start + end) >> 1;
                //cout << "smaller:" << mid << endl;
                if ( mid == start ) return mid;
            }
        }            
    }
    
};