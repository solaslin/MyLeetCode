//
// https://leetcode.com/problems/two-sum/description/
//

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {

        for (int i = 0; i < nums.size(); ++i)
        {
            int expect = target - nums[i];
            for (int j = i + 1; j < nums.size(); ++j)
            {
                if (nums[j] == expect)
                    return vector<int>{i,j};
            }
        }
    }
};