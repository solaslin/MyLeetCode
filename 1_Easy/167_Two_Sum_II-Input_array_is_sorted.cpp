//
// https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/description/
//

class Solution {
public:
    vector<int> twoSum(vector<int>& numbers, int target) {
        int len = numbers.size();
        vector<int> ret;
        for (int i = 0; i < len; ++i)
        {
            for (int j = i+1; j < len; ++j)
            {
                int sum = numbers[i] + numbers[j];
                if ( sum == target )
                {
                    ret.push_back(i+1);
                    ret.push_back(j+1);
                }
                if ( sum > target ) break;
            }
        }
        return ret;
    }
};