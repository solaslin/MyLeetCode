//
// https://leetcode.com/problems/count-primes/description/
//

class Solution {
public:
    int countPrimes(int n) {
        
        if (n <= 2) return 0;
        vector<bool> truthTable(n, true);        
        truthTable[0] = truthTable[0] = false;
        int counts = n - 2;    // 0~n-1, remove 0,1
        
        // refine: use i*i
        for (int i = 2; i*i < n; ++i)
        {
            if (truthTable[i])
            {
                for (int j = i*i; j < n; j += i )
                {
                    if (truthTable[j])
                    {
                        truthTable[j] = false;
                        --counts;
                    }                    
                }
            }
        }
        
        return counts;
    }
};