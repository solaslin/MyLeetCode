//
// https://leetcode.com/problems/binary-tree-level-order-traversal-ii/description/
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrderBottom(TreeNode* root) {
        vector<vector<int>> ret;
        
        if (!root) return ret;
        queue<TreeNode*> bfsList;                
        bfsList.push(root);
        while ( !bfsList.empty() )
        {            
            vector<int> level;
            int len = bfsList.size();
            for (int i = 0; i < len; ++i )
            {
                TreeNode* node = bfsList.front();
                if ( node->left ) bfsList.push( node->left );
                if ( node->right ) bfsList.push( node->right );
                bfsList.pop();
                level.push_back(node->val);
            }
            ret.push_back(level);
        }
        reverse(ret.begin(), ret.end());
        return ret;
    }
};