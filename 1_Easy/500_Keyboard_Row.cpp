//
// https://leetcode.com/problems/keyboard-row/description/
//

class Solution {
public:
    Solution()
    {
        table['q']=table['w']=table['e']=table['r']=table['t']=table['y']=table['u']=table['i']=table['o']=table['p']=1;
        table['a']=table['s']=table['d']=table['f']=table['g']=table['h']=table['j']=table['k']=table['l']=2;
        table['z']=table['x']=table['c']=table['v']=table['b']=table['n']=table['m']=3;
    }
    
    vector<string> findWords(vector<string>& words) 
    {
        vector<string> ret;
        for (auto& s : words)
        {
            string lowerS = s;
            std::transform(lowerS.begin(), lowerS.end(), lowerS.begin(), ::tolower);
            
            int len = lowerS.length();
            if (len == 0)
            {
                ret.push_back( s );
                continue;
            }
            
            int row = table[ lowerS[0] ];
            int i;
            for ( i = 1; i < len; ++i)
            {
                if (table[ lowerS[i] ] != row)
                    break;
            }
            
            if ( i == len ) 
                ret.push_back( s );
        }
        
        return ret;
    }
private:
    std::map<char, int> table;
};