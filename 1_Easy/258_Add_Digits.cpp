//
// https://leetcode.com/problems/add-digits/description/
//

class Solution {
public:
    int addDigits(int num) {
        
        // https://en.wikipedia.org/wiki/Digital_root
        // however, this is a math problem & solution..
        if (num==0) return 0;        
        int ans = num % 9;
        return ans? ans : 9;
        
        // pure programming method:
        //
    }
};