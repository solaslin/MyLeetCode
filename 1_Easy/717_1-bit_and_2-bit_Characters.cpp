//
// https://leetcode.com/problems/1-bit-and-2-bit-characters/description/
//

static int x = []() { 
std::ios::sync_with_stdio(false); 
cin.tie(NULL); 
return 0; 
}();

class Solution {
public:
    bool isOneBitCharacter(vector<int>& bits) 
    {
        int len = bits.size();
        if (len == 1) return true;
        if (bits[len-2] == 0) return true;  // end with "00"
        
        // end with "10"
        if (len == 2) return false;
        return ! isValidString(bits, len-3);
        
        //bool isValid = isValidString(bits, len-3);
        //if (isValid) return false;        
    }
    
private:
    bool isValidString(vector<int>& bits, int lastIdx)
    {
        if (lastIdx < 0) return true;        
        if (lastIdx == 0) return (bits[lastIdx] == 0);  // only "0" is valid, "1" is invalid
        
        // lastIdx >= 1
        if (bits[lastIdx] == 1)
        {
            if (bits[lastIdx-1] == 0) 
                return false;  // end with "01", invalid
            return 
                isValidString(bits, lastIdx-2);   // end with "11", check valid by aheading 2
        }
        else
        {
            if (bits[lastIdx-1] == 0) 
                return isValidString(bits, lastIdx-1);   // end with "00", check valid by aheading 1
            else 
                return isValidString(bits, lastIdx-1) || isValidString(bits, lastIdx-2); // end with "10", check valid by aheading 1 or 2
        }
    }
    
};