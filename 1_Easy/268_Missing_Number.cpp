//
// https://leetcode.com/problems/missing-number/description/
//

class Solution {
public:
    int missingNumber(vector<int>& nums) {
        
        /* same concept with summation:
        int fullXOR = nums.size();
        int actualXOR = 0;
        for (int i = 0; i < nums.size(); ++i)
        {
            actualXOR ^= nums[i];
            fullXOR ^= i;    
        }
        
        return actualXOR ^ fullXOR;
        */
        
        // more simple coding:
        int result = nums.size();
        for (int i = 0; i < nums.size(); ++i )
        {
            result ^= (nums[i] ^ i);
        }
        return result;

        
        /*
        int fullSum = nums.size();
        int actualSum = 0;
        for (int i = 0; i < nums.size(); ++i)
        {
            actualSum += nums[i];
            fullSum += i;            
        }
        
        return fullSum - actualSum;
        */
    }
};