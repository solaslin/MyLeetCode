//
// https://leetcode.com/problems/self-dividing-numbers/description/
//

class Solution {
public:
    vector<int> selfDividingNumbers(int left, int right) {
        vector<int> ret;
        
        for (int num = left; num <= right; ++num )
        {
            if ( isSelfDividing(num) ) ret.push_back(num);
        }
        
        return ret;
    }
    
    bool isSelfDividing(int num)
    {
        int q = num;
        while (q)
        {
            int digit = q % 10;
            if ( !digit || num % digit ) return false;
            q /= 10;
        }
        return true;
    }
};