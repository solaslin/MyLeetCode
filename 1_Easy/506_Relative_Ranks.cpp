//
// https://leetcode.com/problems/relative-ranks/description/
//

class Solution {
public:
    vector<string> findRelativeRanks(vector<int>& nums) {
        int len = nums.size();
        vector<pair<int, int>> scoreAndId(len);
        vector<string> ret(len);
                
        for (int i = 0; i < len; ++i)
            scoreAndId[i] = make_pair(-nums[i],i);
        
        //sort( scoreAndId.begin(), scoreAndId.end(), [](pair<int,int>& a, pair<int,int>& b) { return a.first > b.first;} );
        sort( scoreAndId.begin(), scoreAndId.end());
        
        if (len >= 1) ret[ scoreAndId[0].second ] = "Gold Medal";
        if (len >= 2) ret[ scoreAndId[1].second ] = "Silver Medal";
        if (len >= 3) ret[ scoreAndId[2].second ] = "Bronze Medal";
        for (int i = 3; i < len; ++i )
            ret[ scoreAndId[i].second ] = to_string(i+1);
        
        return ret;
    }
};