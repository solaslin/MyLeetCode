//
// https://leetcode.com/problems/construct-the-rectangle/description/
//

class Solution {
public:
    vector<int> constructRectangle(int area) {
        vector<int> ret;
        int largestW = (int)sqrt(area);
        for (int i = largestW; i >=1; --i )
        {
            if (area % i == 0)
            {
                ret.push_back(area/i);
                ret.push_back(i);                
                break;
            }
        }
        
        return ret;
    }
};