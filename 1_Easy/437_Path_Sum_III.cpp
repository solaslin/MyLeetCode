//
// https://leetcode.com/problems/path-sum-iii/description/
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int pathSum(TreeNode* root, int sum) {
        return recurs_pathSum(root, sum, true) + recurs_pathSum(root, sum, false);
    }
    
    private:
    int recurs_pathSum(TreeNode* root, int sum, bool includeSelf)
    {
        if (!root) return 0;
        
        //cout << "call: " << root->val << "," << sum << "," << includeSelf << endl;
        if ( root->left || root->right )
        {
            if (includeSelf == true)
            {
                //if (root->val == sum) cout << "a:" << root->val << endl;
                
                return recurs_pathSum( root->left,  sum-root->val, true ) +
                       recurs_pathSum( root->right, sum-root->val, true ) +
                       ((root->val == sum)? 1 : 0);
            }
            
            else
            {
                return recurs_pathSum( root->left,  sum, true ) +
                       recurs_pathSum( root->right, sum, true ) +
                       recurs_pathSum( root->left,  sum, false ) + 
                       recurs_pathSum( root->right, sum, false );    
            }
        }
        
        if (includeSelf) return (root->val == sum)? 1 : 0;        
        return 0;
        //if (root->val == sum) cout << includeSelf << "," << root->val << ", sum=" << sum << endl;
        //return (root->val == sum)? 1: 0;
    }
};