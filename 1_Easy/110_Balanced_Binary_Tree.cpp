//
// https://leetcode.com/problems/balanced-binary-tree/description/
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isBalanced(TreeNode* root) {
        int height;
        return getBalancedAndHeight(root, height);
    }
    
    bool getBalancedAndHeight(TreeNode* node, int& height)
    {
        if ( !node )
        {
            height = 0;
            return true;
        }
        
        int hLeft, hRight;
        if ( getBalancedAndHeight(node->left, hLeft) == false )
            return false;
        if ( getBalancedAndHeight(node->right, hRight) == false )
            return false;
        
        if ( abs(hLeft - hRight) > 1 )
        //if ( (hLeft - hRight) > 1 || (hLeft - hRight) < -1)
            return false;
        
        height = max(hLeft, hRight) + 1;
        return true;
    }
};