//
// https://leetcode.com/problems/base-7/description/
//

class Solution {
public:
    string convertToBase7(int num) {
                
        bool isNeg = ( num < 0 );
        if ( isNeg ) num = -num;
        
        // method1: by string char
        string ret;
        while ( num >= 7 )            
        {
            ret += to_string(num % 7);
            num /= 7;
        }
        ret += to_string(num);
        
        if (isNeg) ret += "-";        
        std::reverse( ret.begin(), ret.end() );        
        return ret;
        
        // method2: by final to_string
        /*
        int base7 = 0;
        int factor = 1;
        while ( num >= 7 )            
        {
            base7 += (num % 7) * factor;
            factor *= 10;
            num /= 7;
        }
        base7 += num * factor;
        
        return (isNeg)? "-" + to_string(base7) : to_string(base7) ;
        */
    }
};