//
// https://leetcode.com/problems/largest-number-at-least-twice-of-others/description/
//

class Solution {
public:
    int dominantIndex(vector<int>& nums) 
    {        
        int len = nums.size();
        if (len==1) return 0;
        
        int maxV = nums[0];
        int secMaxV = min(nums[0],nums[1]);    
        
        int ret = 0;        
        for ( int i = 1; i < len; ++i )
        {
            int n = nums[i];
            if ( nums[i] > maxV )
            {
                secMaxV = maxV;
                maxV = n;
                ret = i;
            }
            else
            {
                secMaxV = max(secMaxV,n);
            }            
        }
        
        //std::cout << maxV << "," << secMaxV;
        return (maxV >= (secMaxV << 1))? ret : -1;
    
    }
};