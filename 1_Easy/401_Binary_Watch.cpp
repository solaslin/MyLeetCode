//
// https://leetcode.com/problems/binary-watch/description/
//

class Solution {
public:
    Solution()
    {
        _possibleNums.push_back( unordered_set<int>{0} );   // 0 LED
        _possibleNums.push_back( unordered_set<int>{1,2,4,8,16,32} );   // 1 LED
        _possibleNums.push_back( unordered_set<int>{3,5,6,9,10,12,17,18,20,24,33,34,36,40,48} );   // 2 LED
        _possibleNums.push_back( unordered_set<int>{7,11,13,14,19,21,22,25,26,28,35,37,38,41,42,44,49,50,52,56} );   // 3 LED
        _possibleNums.push_back( unordered_set<int>{15,23,27,29,30,39,43,45,46,51,53,54,57,58} );   // 4 LED
        _possibleNums.push_back( unordered_set<int>{31,47,55,59} );   // 5 LED
    }
    
    vector<string> readBinaryWatch(int num) {
        
        vector<string> ret;
        for ( int h = 0; h <= 3; ++h )
        {
            int m = num - h;
            if (m < 0) break;
            if (m > 5) continue;
            unordered_set<int>& hrNums  = _possibleNums[h];
            unordered_set<int>& minNums = _possibleNums[m];
            for ( int hr : hrNums )
            {
                if (hr > 11) continue;
                string hrStr = to_string(hr) + ":";
                for ( int minute : minNums )
                {
                    string minStr = (minute < 10)? "0" + to_string(minute) : to_string(minute);
                    ret.push_back(hrStr + minStr);
                }
            }            
        }
        
        return ret;
    }
private:
    vector< unordered_set<int> > _possibleNums;
    
};