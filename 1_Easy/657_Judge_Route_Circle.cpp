//
// https://leetcode.com/problems/judge-route-circle/description/
//


static int x = []() { 
std::ios::sync_with_stdio(false); 
cin.tie(NULL); 
return 0; 
}();

class Solution {
public:
    bool judgeCircle(string moves) {
        std::vector<int> counts(128,0);        
        
        //for (size_t i = 0; i < moves.size(); ++i)
        //    ++counts[ moves[i] ];
        // auto is way faster !!??
        for ( auto c : moves )
            ++counts[c];        

        
        return ( counts['U']==counts['D'] && counts['R']==counts['L'] );
        /*    
        int x = 0, y = 0;
        for (size_t i = 0; i < moves.size(); ++i)
        {
            if (moves[i] == 'U' ) ++y;
            else if (moves[i] == 'D' ) --y;
            else if (moves[i] == 'R' ) ++x;
            else if (moves[i] == 'L' ) --x;
        }
        return (x == 0)&&(y == 0);
        */
    }
};