//
// https://leetcode.com/problems/detect-capital/description/
//

class Solution {
public:
    bool detectCapitalUse(string word) {
     
        bool firstIsCap = (word[0] - 'a') < 0;
        //cout << firstIsCap << endl;
        
        if (word.size() < 2) return true;
        bool secIsCap = (word[1] - 'a') < 0;
        //cout << secIsCap << endl;
        
        for (int i = 1; i < word.size(); ++i )
        {
            // first is lower case, but word[i] is capital
            if (!firstIsCap)
            {
                if (word[i] -'a' < 0) return false;
            }
            else
            {
                if (secIsCap != (word[i] -'a' < 0)) return false;
            }            
        }
        
        return true;
    }
};