//
// https://leetcode.com/problems/minimum-depth-of-binary-tree/description/
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int minDepth(TreeNode* root) {
        
        if (!root) return 0;
        queue<TreeNode*> bfsList;                
        bfsList.push(root);
        int depth = 0;
        int len = bfsList.size();
        while ( len )
        {            
            ++depth;
            for (int i = 0; i < len; ++i )
            {
                TreeNode* node = bfsList.front();
                if ( !node->left && !node->right ) return depth;
                if ( node->left ) bfsList.push( node->left );
                if ( node->right ) bfsList.push( node->right );
                bfsList.pop();
            }
            len = bfsList.size();
        }
    }
};