//
// https://leetcode.com/problems/student-attendance-record-i/description/
//

class Solution {
public:
    bool checkRecord(string s) {
        int numA = 0, contNumL = 0;
        //int len = s.size();
        for (int i = 0; i < s.size(); ++i)
        {
            if (s[i] == 'A')
            {
                ++numA;
                if (numA > 1) return false;
            }
            if (s[i] == 'L')
            {
                ++contNumL;
                if (contNumL > 2) return false;
            }
            else
            {
                contNumL = 0;
            }
            
            //if (numA == 2 || contNumL == 3) return false;
        }
        return true;
    }
};