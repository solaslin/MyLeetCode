//
// https://leetcode.com/problems/merge-two-binary-trees/description/
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* mergeTrees(TreeNode* t1, TreeNode* t2) {

        // method 1: maybe faster but I don't think it's right since it modifies the input....        
        if (!t1) return t2;
        if (!t2) return t1;
        
        t1->val += t2->val;
        t1->right = mergeTrees(t1->right, t2->right);
        t1->left  = mergeTrees(t1->left, t2->left);
        return t1;
        

        // method 2: return a brandnew tree and remains the input, i prefer since it's safer
        /*
        if ( !t1 && !t2 ) 
            return nullptr;
        
        TreeNode* ret = new TreeNode( (t1? t1->val : 0) + (t2? t2->val : 0));
        ret->right = mergeTrees( t1? t1->right : nullptr, t2? t2->right : nullptr);
        ret->left  = mergeTrees( t1? t1->left  : nullptr, t2? t2->left  : nullptr);        
        return ret;
        */
    }
};