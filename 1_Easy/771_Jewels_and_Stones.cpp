//
// https://leetcode.com/problems/jewels-and-stones/description/
//

class Solution {
public:
    int numJewelsInStones(string J, string S) {
        
        int numS = S.length();
        int numJ = J.length();
        int ret = 0;
        
        for (int i = 0; i < numS; ++i)
        {
            for (int j = 0; j < numJ; ++j)
            {
                if ( S[i] == J[j] )
                {
                    ++ret;
                    break;
                }
            }
        }
        
        return ret;
    }
};