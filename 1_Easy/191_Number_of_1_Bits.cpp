//
// https://leetcode.com/problems/number-of-1-bits/description/
//

class Solution {
public:
    int hammingWeight(uint32_t n) {
        
        int ret = 0;
        while (n)
        {
            ++ret;
            n &= (n-1);     // remove last 1
        }
        return ret;
    }
};