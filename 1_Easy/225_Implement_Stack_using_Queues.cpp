//
// https://leetcode.com/problems/implement-stack-using-queues/description/
//

class MyStack {
public:
    /** Initialize your data structure here. */
    MyStack() {
        
    }
    
    /** Push element x onto stack. */
    void push(int x) {
        //_doubleQ[curOpID].push(x);
        _lastPush = x;
        _queue.push(x);
    }
    
    /** Removes the element on top of the stack and returns that element. */
    int pop() {
        queue<int> dupQ;
        int len = _queue.size();
        
        for (int i = 1; i <= len - 2; ++i)
        {
            dupQ.push( _queue.front() );
            _queue.pop();
        }
        
        if (len >= 2)
        {
            _lastPush = _queue.front();
            _queue.pop();
            dupQ.push( _lastPush );
        }        
        
        int ret = _queue.front();
        _queue = dupQ;
        return ret;
    }
    
    /** Get the top element. */
    int top() {
        return _lastPush;
    }
    
    /** Returns whether the stack is empty. */
    bool empty() {
        return _queue.empty();
    }
    
private:
    queue<int> _queue;
    int _curOpID = 0;
    int _lastPush;
};

/**
 * Your MyStack object will be instantiated and called as such:
 * MyStack obj = new MyStack();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.top();
 * bool param_4 = obj.empty();
 */