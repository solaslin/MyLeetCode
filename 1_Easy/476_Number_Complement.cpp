//
// https://leetcode.com/problems/number-complement/description/
//

class Solution {
public:
    int findComplement(int num) {
        
        int ret = 0;
        int digitID = 0;
        while (num)
        {
            if ( !(num & 1) ) ret |= 1 << digitID;
            ++digitID;
            num >>= 1;
        }

        return ret;
    }
};