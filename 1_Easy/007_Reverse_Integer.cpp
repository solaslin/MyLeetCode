//
// https://leetcode.com/problems/reverse-integer/description/
//

static int x = []() { 
    std::ios::sync_with_stdio(false); 
    cin.tie(NULL);  
    return 0; 
}();

class Solution {
public:
    int reverse(int x) {        
        
        int sign = x >> 31;
        if (sign) x = (1<<32) - x;
        //std::cout << x << std::endl;
        long ret = 0;
        do
        {
            int digit = x % 10;
            ret = ret * 10 + digit;
            if (ret >> 31) return 0;
            x /= 10;            
        } while( x > 0 );
        
        //std::cout << ret;
        if (sign) ret = (1<<32) - ret;        
        return ret;
    }
};