//
// https://leetcode.com/problems/image-smoother/description/
//

class Solution {
public:
    vector<vector<int>> imageSmoother(vector<vector<int>>& M) {
        int row = M.size();
        int col = M[0].size();
        
        vector<vector<int>> retM( row, vector<int>(col) );
        for (int x = 0; x < row; ++x)
        {
            for (int y = 0; y < col; ++y)
            {
                int counts = 0;
                for ( int dx = -1; dx <=1; ++dx)
                {
                    if (x + dx < 0 || x + dx >= row ) continue;
                    for ( int dy = -1; dy <=1; ++dy)
                    {
                        if (y + dy < 0 || y + dy >= col ) continue;                        
                        retM[x][y] += M[x+dx][y+dy];
                        ++counts;
                    }
                }
                retM[x][y] /= counts;
            }
        }
        
        return retM;
    }
};