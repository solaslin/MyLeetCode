//
// https://leetcode.com/problems/unique-morse-code-words/description/
//

string codeTable[26] = {".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};

class Solution {
public:
    int uniqueMorseRepresentations(vector<string>& words) {
        
        unordered_set<string> morseCode;
        
        for (string s : words)
        {
            string code = "";
            for (char c : s)
                code += codeTable[c-'a'];

            morseCode.insert(code);
        }
        
        return morseCode.size();
    }
};