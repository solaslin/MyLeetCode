//
// https://leetcode.com/problems/count-binary-substrings/description/
//

class Solution {
public:
    int countBinarySubstrings(string s) 
    {        
        int len = s.length();        
        if (len == 1) return 0;
        
        std::vector<int> counts;
        int count = 1;
        for (int i = 0; i < len-1; ++i)
        {
            if ( s[i] == s[i+1] ) 
                ++count;
            else
            {
                counts.push_back(count);
                count = 1;
            }
        }        
        counts.push_back(count);
        
        int result = 0;
        len = counts.size();
        for (int i = 0; i < len-1; ++i)
            result += min(counts[i], counts[i+1]);

        return result;
    }
};