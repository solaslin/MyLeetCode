//
// https://leetcode.com/problems/sum-of-left-leaves/description/
//

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

static string opt =[](){
    std::ios::sync_with_stdio(false);
    cin.tie(NULL);
    return "";
}();

class Solution {
public:
    int sumOfLeftLeaves(TreeNode* root) {
        return recursiveSum(root, false);
    }
    
private:
    int recursiveSum( TreeNode* node, bool isLeft )
    {
        if ( !node ) return 0;
        if ( !node->left && !node->right )  // if is LeafNode
        {
            if (isLeft) return node->val;
            return 0;
        }
        else
        {
            return recursiveSum(node->left, true) + recursiveSum(node->right, false);
        }
    }
};