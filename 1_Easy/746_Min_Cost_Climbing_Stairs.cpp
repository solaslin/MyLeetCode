//
// https://leetcode.com/problems/min-cost-climbing-stairs/description/
//

class Solution {
public:
    int minCostClimbingStairs(vector<int>& cost) {
        int numSteps = cost.size();
        vector<int> minCostAtHere(numSteps+1);
        if (numSteps == 2) return min(cost[0], cost[1]);
        
        for (int i = 2; i < numSteps + 1; ++i)
        {
            minCostAtHere[i] = min( minCostAtHere[i-2] + cost[i-2], minCostAtHere[i-1] + cost[i-1]);
        }
        return minCostAtHere[numSteps];
    }
};